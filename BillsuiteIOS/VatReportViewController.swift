//
//  VatReportViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 25/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//
struct vat :Decodable{
    var vat_collected: String?
    var vatremitted: String?
    var gov: String?
}
import UIKit
import  Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
class VatReportViewController: UIViewController,DataSource {
    @IBOutlet var txtgov: UITextField!
    @IBOutlet var txtrem: UITextField!
    @IBOutlet var txtcollect: UITextField!
    var tag:Int?
    var username1:String?
    var password1:String?
    var varData:[vat] = [vat]()
    func getDate(date: String) {
        if tag == 1{
            txtfrm.text = date
            print(txtfrm.text)
        }
        else{
            txtto.text = date
        }
    }
    @IBOutlet var txtto: UITextField!
    @IBOutlet var txtfrm: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtgov.isUserInteractionEnabled = false
        txtrem.isUserInteractionEnabled = false
        txtcollect.isUserInteractionEnabled = false
        txtto.attributedPlaceholder = NSAttributedString(string: " Select To Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtfrm.attributedPlaceholder = NSAttributedString(string: "Select From Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtfrm.isUserInteractionEnabled = false
        txtto.isUserInteractionEnabled = false
        var dataStore = dataFromUserdefaults()
        username1 = dataStore["name"]
        password1 = dataStore["pass"]
    }
    @IBAction func btnfrm(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnto(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btnvat(_ sender: UIButton) {
        getVatReport()
    }
    func getVatReport(){
        let url =  "http://nuvactech-001-site13.etempurl.com/ios_new_vat.php"
        Alamofire.request(url, method: .post, parameters: ["from":txtfrm.text!,"to":txtto.text!,"username" : username1!,"password" : password1!]).responseData { (res) in
            if res.result.isSuccess{
                print("Success")
                do{
                    let result = try JSONDecoder().decode([vat].self, from: res.data!)
                    print(result)
                    if let status = result[0] as? String{
                        print("data ella")
                        return
                    }
                    print(result[0].vat_collected!)
                    self.txtcollect.text = result[0].vat_collected
                    self.txtrem.text = result[0].vatremitted
                    self.txtgov.text = result[0].gov
                }
                catch{
                    print(error.localizedDescription)
                    let aler = alert(message: "invalid date", Title: "no data found")
                    self.present(aler, animated: true, completion: nil)
                }
            }
        }
    }
}
