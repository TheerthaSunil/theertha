//
//  MerchantCustomViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 22/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//
protocol merchantSentData {
    func getName(name:String)
    
    
}
protocol hotelSendData {
    func getHotel(hotelname:String)
    
    
}
struct Hotel :Decodable{
    var name: String?
    var email:String?
}
struct purchaseData : Decodable {
    var merchant:String?
    
}
import UIKit
import Alamofire
import IQKeyboardManagerSwift

class MerchantCustomViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    //
    var lis: [Hotel] = [Hotel]()
    var merchantData:[purchaseData] = [purchaseData]()
    var delegate:merchantSentData?
    var delegate1:hotelSendData?
    var username2:String = ""
    var password2:String?
    var merchantName:String?
    var pass:Int?
    var hotelName:String?
    var hotelemail:String?
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var num:Int?
        if pass == 0{
        num = merchantData.count
        }
        else if pass == 1{
            num = lis.count
        }
        return num!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var numb:String?
        if pass == 0{
            numb = merchantData[row].merchant!
            }
        else if pass == 1{
            numb = lis[row].name
        }
        return numb!
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var numbb: Int?
        if pass == 0{
            merchantName = merchantData[row].merchant!
        }
        else if pass == 1{
            hotelName = lis[row].name
            
        }
    }
    @IBOutlet var pickermerchant: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        var Store = dataFromUserdefaults()
        username2 = Store["name"]!
        password2 = Store["pass"]!
        if pass == 0{
             PurchaseDetail()
        }
        else if pass == 1{
            loadFirmData()
        }

       
    }

   
    @IBAction func btnselect(_ sender: UIButton) {
        if pass == 0{
            delegate?.getName(name: merchantName!)
            dismiss(animated: true, completion: nil)
        }
        else if pass == 1{
            delegate1?.getHotel(hotelname: hotelName!)
            dismiss(animated: true, completion: nil)
        }
    }
    func PurchaseDetail(){
        let url = "http://nuvactech-001-site13.etempurl.com/ios_new_merchant.php"
        print(".....")
        Alamofire.request(url, method: HTTPMethod.post, parameters: ["username":username2,"password":password2!]).responseJSON { (res) in
            if res.result.isSuccess{
                print("success")
                do{
                    self.merchantData = try JSONDecoder().decode([purchaseData].self, from: res.data!)
                    print(self.merchantData)
                   self.pickermerchant.reloadAllComponents()
                    self.merchantName = self.merchantData[0].merchant
                    
                }
                catch{
                    print("Error\(error.localizedDescription)")
                }
            }
            else{
                print("failed")
            }
        }
        
    }
    func loadFirmData(){
        let firmUrl = "http://nuvactech-001-site13.etempurl.com/ios_hotel.php"
        Alamofire.request(firmUrl, method: .post, parameters: ["username" : username2,"password": password2!]).responseJSON { (response) in
            if response.result.isSuccess{
                do{
                    let result = try JSONSerialization.jsonObject(with: response.data!) as! [String : AnyObject]
                    let hotel = result["hotel"]!
                    let resultData = hotel as! [AnyObject]
                    for branches in resultData{
                        self.lis.append(Hotel(name: branches["name"] as! String, email: branches["email"] as! String))
                        print(branches["name"])
                    }
                    self.pickermerchant.reloadAllComponents()
                     self.hotelName = self.lis[0].name
                }catch{
                    print("Failed")
                }
            }
        }
    }
    
}
