//
//  purchaseTableViewCell.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 24/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//

import UIKit

class purchaseTableViewCell: UITableViewCell {

    @IBOutlet var lblremark1: UILabel!
    @IBOutlet var lblpay1: UILabel!
    @IBOutlet var lblremark: UILabel!
    @IBOutlet var lblpay: UILabel!
    @IBOutlet var lblvat1: UILabel!
    @IBOutlet var lblvat: UILabel!
    @IBOutlet var lblamount1: UILabel!
    @IBOutlet var lblamount: UILabel!
    @IBOutlet var lblinvoice1: UILabel!
    @IBOutlet var lblinvoice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
