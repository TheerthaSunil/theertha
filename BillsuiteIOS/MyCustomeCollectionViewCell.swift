//
//  MyCustomeCollectionViewCell.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 21/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//

import UIKit

class MyCustomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imagePhoto: UIImageView!
    @IBOutlet var labtitle: UILabel!
    
    @IBOutlet var lbldest: UILabel!
    
    @IBOutlet var lblid: UILabel!
    
    
    @IBOutlet var lbladdr: UILabel!
    
}
