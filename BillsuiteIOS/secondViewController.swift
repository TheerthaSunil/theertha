//
//  secondViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 17/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//
struct saveData:Encodable,Decodable {
    var name:String?
    var pass:String?
}
struct details :Encodable,Decodable{
    var name: String?
    var email:String?
}
protocol params {
    func getparams(name:String,pass:String)
}
import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
class secondViewController: BaseViewController,DataSource,UIPickerViewDelegate,UIPickerViewDataSource
{
    var lis: [details] = [details]()
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return lis[row].name
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return lis.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtbranch.text = lis[row].name
    }
    var loginId:[saveData] = [saveData]()
    var defaults = UserDefaults.standard
    var filePath = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    var paramdetails:params?
    func getDate(date: String) {
        if tag == 1{
            txtfromdate.text = date
        }
        else{
            txttodate.text = date
        }
    }
    var hotelname:String?
    var password1:String?
    var username1:String?
    var emailid:String?
    var list:[dataSource]=[dataSource]()
    @IBOutlet var txtbranch: UITextField!
    @IBOutlet var govtxt: UITextField!
    @IBOutlet var remittedtxt: UITextField!
    @IBOutlet var collectedtxt: UITextField!
    @IBOutlet var purchasetxt: UITextField!
    @IBOutlet var salestxt: UITextField!
    @IBOutlet var gov: UILabel!
    @IBOutlet var remittence: UILabel!
    @IBOutlet var tax: UILabel!
    @IBOutlet var vat: UILabel!
    @IBOutlet var purchase: UILabel!
    @IBOutlet var sales: UILabel!
    @IBOutlet var txttodate: UITextField!
    @IBOutlet var input: UITextField!
    @IBOutlet var firmpicker: UIPickerView!
    @IBOutlet var txtfromdate: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txttodate.attributedPlaceholder = NSAttributedString(string: "Select To Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtfromdate.attributedPlaceholder = NSAttributedString(string: "Select From Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtfromdate.isUserInteractionEnabled = false
        txttodate.isUserInteractionEnabled = false
        salestxt.isUserInteractionEnabled = false
        purchasetxt.isUserInteractionEnabled = false
        collectedtxt.isUserInteractionEnabled = false
        remittedtxt.isUserInteractionEnabled = false
        govtxt.isUserInteractionEnabled = false
        var dataStore = dataFromUserdefaults()
        username1 = dataStore["name"]
        password1 = dataStore["pass"]
        let noteDetail = saveData(name: username1, pass: password1)
        self.loginId.append(noteDetail)
        print(FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask))
        let encoder = PropertyListEncoder()
        do{
            let encodedData = try encoder.encode(noteDetail)
            
            try encodedData.write(to: self.filePath!)
            print(encodedData)
            print(filePath)
        }catch{
            print("Error while encoding \(error.localizedDescription)")
        }
        if let username1 = username1{
            loadData()
        }
        addSlideMenuButton()
        loadFirmData()
        if let username1 = username1{
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func loadData(){
        let url = "http://nuvactech-001-site13.etempurl.com/ios_new_admin_index1.php"
        Alamofire.request(url, method: .post, parameters: ["username" : username1!,"password" : password1!]).responseJSON { (res) in
            if res.result.isSuccess{
                print("successs")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as! [String:Any]
                    for (key,value) in result["0"] as! [String : Any]{
                        print(key)
                        self.list.append(dataSource(key: key, value: value))
                    }
                    self.salestxt.text = self.list[0].value as? String
                    self.collectedtxt.text = self.list[3].value as? String
                    self.govtxt.text = self.list[5].value as? String
                    self.remittedtxt.text = self.list[4].value as? String
                    print(self.list[4].value)
                    self.purchasetxt.text = self.list[1].value as? String
                    print(result["status"]!)
                }catch{
                    print("Error\(error.localizedDescription)")
                }
                
            }else{
                print("sdfgh")
            }
            
        }
    }
    var tag:Int?
    @IBAction func btnfrm(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnto(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
        }
    }
    func fiterData(){
        let url = "http://nuvactech-001-site13.etempurl.com/ios_new_admin_index1.php"
        Alamofire.request(url, method: .post, parameters: ["from":txtfromdate.text!,"to":txttodate.text!,"client":txtbranch.text!,"username" : username1!,"password" : password1!]).responseJSON { (res) in
            if res.result.isSuccess{
                print("successs")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as! [String:Any]
                    print(result)
                    if (result["status"] != nil){
                        
                        let msgbox = alert(message: "Invalid date",Title: "Alert")
                        self.present(msgbox, animated: true, completion: nil)
                        print(result["status"]!)
                        self.salestxt.text = ""
                        self.collectedtxt.text = ""
                        self.remittedtxt.text = ""
                        self.purchasetxt.text = ""
                        self.govtxt.text = ""
                    }
                    else{
                        self.list.removeAll()
                        for (key,value) in result["message"] as! [String : Any]{
                            print(key, value)
                            self.list.append(dataSource(key: key, value: value))
                        }
                        self.salestxt.text = self.list[0].value as? String
                        self.collectedtxt.text = self.list[3].value as? String
                        self.govtxt.text = self.list[5].value as? String
                        self.remittedtxt.text = self.list[4].value as? String
                        self.purchasetxt.text = self.list[1].value as? String
                    }
                }catch{
                    print("Error\(error.localizedDescription)")
                }
            }else{
                print("sdfgh")
            }
        }
    }
    
    @IBAction func btnfilter(_ sender: UIButton) {
        fiterData()
    }
    func encodeData()
    {
        let encoder = PropertyListEncoder()
        do{
            let encodedData = try encoder.encode(self.loginId)
            try encodedData.write(to: self.filePath!)
        }catch{
            print("Error while encoding \(error.localizedDescription)")
        }
    }
    func loadFirmData(){
        let firmUrl = "http://nuvactech-001-site13.etempurl.com/ios_hotel.php"
        Alamofire.request(firmUrl, method: .post, parameters: ["username" : username1!,"password": password1!]).responseJSON { (response) in
            if response.result.isSuccess{
                do{
                    let result = try JSONSerialization.jsonObject(with: response.data!) as! [String : AnyObject]
                    let hotel = result["hotel"]!
                    let resultData = hotel as! [AnyObject]
                    for branches in resultData{
                        self.lis.append(details(name: branches["name"] as! String, email: branches["email"] as! String))
                        print(branches["name"])
                    }
                    self.firmpicker.reloadAllComponents()
                    
                }catch{
                    print("Failed")
                }
            }
        }
    }
}
