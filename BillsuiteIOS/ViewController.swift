//
//  ViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 17/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
struct logDetail:Encodable,Decodable{
    var password:String?
    var username:String?
}
import Alamofire
import SwiftyJSON
import UIKit
class ViewController: UIViewController {
    //new version
    var log:[logDetail] = [logDetail]()
    @IBOutlet var passwordtxt: UITextField!
    @IBOutlet var usertxt: UITextField!
    @IBOutlet var imglogo: UIImageView!
    var defaults = UserDefaults.standard
    var filePath = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordtxt.layer.cornerRadius = passwordtxt.frame.height*0.5
        usertxt.attributedPlaceholder = NSAttributedString(string: "Enter UserName",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        passwordtxt.attributedPlaceholder = NSAttributedString(string: "Enter Your Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        passwordtxt.layer.cornerRadius = 5
        passwordtxt.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        passwordtxt.layer.borderWidth = 0.5
        passwordtxt.clipsToBounds = true
        UIView.animate(withDuration: 3) {
            self.imglogo.alpha = 1
        }
    }
    @IBAction func loginbtn(_ sender: UIButton) {
        login()
    }
    func login(){
        let url  = "http://nuvactech-001-site13.etempurl.com/ios_new_login.php"
        Alamofire.request(url, method: HTTPMethod.post, parameters: ["username" : usertxt.text!,"password":passwordtxt.text!,"appcategory":"BillSuite-RMS"]).responseJSON { (response) in
            print("Loaded")
            if response.result.isSuccess {
                print("Success")
                do {
                    let data = try JSON(data: response.data!)
                    
                    if data["status"].stringValue == "true" {
                        print("Auth Successs")
                        
                        let Details = saveData(name: self.usertxt.text, pass: self.passwordtxt.text)
                        let encoder = PropertyListEncoder()
                        do{
                            let encodedData = try encoder.encode(Details)
                            
                            try encodedData.write(to: self.filePath!)
                            print(encodedData)
                            print(self.filePath)
                            print(FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask))
                        }
                        catch{
                            print("Error while encoding \(error.localizedDescription)")
                        }
                        
                        self.performSegue(withIdentifier: "pass", sender: self)
                    }
                }catch {
                    
                    print("Error while \(error.localizedDescription)")
                }
                
            }else{
                print("Failed")
            }
            
        }
    }
    
    
}

