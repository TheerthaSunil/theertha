//
//  HistoryViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 26/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
struct History {
    var fromdate:String?
    var todate: String?
    var regno:String?
    var branch:String?
    var status:String?
}
class HistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,DataSource,hotelSendData{
    func getHotel(hotelname: String) {
        txthotel.text = hotelname
        print(hotelname)
    }
    var hisoryDetails:[History] = [History]()
    var password1:String?
    var username1:String?
    var tag:Int?
    func getDate(date: String) {
        if tag == 1{
            txtfrom.text = date
        }
        else{
            txtto.text = date
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hisoryDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")as? historyTableViewCell
        cell?.txtbranch.text = hisoryDetails[indexPath.row].branch
        cell?.txtfromdate.text = hisoryDetails[indexPath.row].fromdate
        cell?.txttodate.text = hisoryDetails[indexPath.row].todate
        cell?.txtstatus.text = hisoryDetails[indexPath.row].status
        cell?.txtregno.text = hisoryDetails[indexPath.row].regno
        return cell!
    }
    @IBOutlet var tblhistory: UITableView!
    @IBOutlet var txthotel: UITextField!
    @IBOutlet var txtto: UITextField!
    @IBOutlet var txtfrom: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfrom.isUserInteractionEnabled = false
        txtto.isUserInteractionEnabled = false
        txtto.attributedPlaceholder = NSAttributedString(string: "Select To Date Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtfrom.attributedPlaceholder = NSAttributedString(string: "Select From Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        tblhistory.backgroundColor = .clear
        tblhistory.isHidden = true
        var dataStore = dataFromUserdefaults()
        username1 = dataStore["name"]
        password1 = dataStore["pass"]
        print(password1!)
        tblhistory.delegate = self
        tblhistory.dataSource = self
    }
    @IBAction func btnfrom(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btnto(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btnhotel(_ sender: UIButton) {
        if let storyboard = storyboard{
            let vc = storyboard.instantiateViewController(withIdentifier:"MerchantVC") as! MerchantCustomViewController
            vc.delegate1 = self
            vc.pass = 1
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnhis(_ sender: UIButton) {
        tblhistory.isHidden = false
        getHistory()
    }
    func getHistory() {
        let url = "http://nuvactech-001-site13.etempurl.com/ios_history.php"
        Alamofire.request(url, method: .post, parameters: ["username" : username1!,"password" : password1!,"hotel":txthotel.text!,"from":txtfrom.text!,"to":txtto.text!]).responseData { (res) in
            if res.result.isSuccess{
                print("success")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as! AnyObject
                    let dat = result["history"] as! [AnyObject]
                    print(dat)
                    for name in dat{
                        print(name["branch"] as!String)
                        print(name["id"] as!Int)
                        self.hisoryDetails.append(History(fromdate: name["fromdate"] as? String, todate: name["todate"] as? String, regno: name["regno"] as? String, branch: name["branch"] as? String, status: name["status"] as? String))
                    }
                    
                    self.tblhistory.reloadData()
                }
                catch{
                    
                    print(error)
                }
            }
            
        }
    }
}
