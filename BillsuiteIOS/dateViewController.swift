//
//  dateViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 18/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//
protocol DataSource {
    func getDate(date:String)
}
struct dataSource{
    var key:String?
    var value:Any?
}
import UIKit
import Alamofire

class dateViewController: UIViewController{
    var data:DataSource?
    var tag:Int?
    var username1:String = ""
    var password1:String = ""
    
    var datacontent:String?
    @IBOutlet var pickerDate: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnGetDate(_ sender: UIButton) {
  let formate = DateFormatter()
        formate.dateFormat = "YYYY-MM-dd"
         var datastore = formate.string(from: pickerDate.date)
        data?.getDate(date: datastore)
        dismiss(animated: true, completion: nil)
        
    }}

