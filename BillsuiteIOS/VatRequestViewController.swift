//
//  VatRequestViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 26/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//
struct Req {
    var id:String?
    var details: String?
    var message:String?
    var fromdate:String?
    var todate: String?
    var email: String?
    var regno:String?
    var branch:String?
}
import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
class VatRequestViewController: UIViewController {
    @IBOutlet var txtto: UITextField!
    @IBOutlet var txtregno: UITextField!
    @IBOutlet var txtmessage: UITextField!
    @IBOutlet var txtid: UITextField!
    @IBOutlet var txtfrom: UITextField!
    @IBOutlet var txtemail: UITextField!
    @IBOutlet var txtdetails: UITextField!
    @IBOutlet var txtbranch: UITextField!
    @IBOutlet var txtreq: UITextField!
    var username1:String?
    var password1:String?
    var ReqVat:[Req] = [Req]()
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfrom.isUserInteractionEnabled = false
        txtto.isUserInteractionEnabled = false
        txtbranch.isUserInteractionEnabled = false
        txtdetails.isUserInteractionEnabled = false
        txtemail.isUserInteractionEnabled = false
        txtid.isUserInteractionEnabled = false
        txtmessage.isUserInteractionEnabled = false
        txtregno.isUserInteractionEnabled = false
        var dataStore = dataFromUserdefaults()
        username1 = dataStore["name"]
        password1 = dataStore["pass"]
        var regStore:String = dataFromVatReq()
        print(".....")
        print(regStore)
        txtreq.text = regStore
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func btnReq(_ sender: UIButton) {
        getReqVat()
    }
    func getReqVat()  {
        let url = "http://nuvactech-001-site13.etempurl.com/ios_requestvat.php"
        Alamofire.request(url, method: .post, parameters: ["username" : username1!,"password" : password1!,"regno":txtreq.text!]).responseData { (res) in
            if res.result.isSuccess{
                print("success")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as! AnyObject
                    let dat = result["data"] as! [AnyObject]
                    print(dat)
                    for content in dat {
                        print(content["branch"] as! String)
                        self.ReqVat.append(Req(id: content["id"] as! String, details: content["details"] as! String, message: content["message"] as! String, fromdate: content["fromdate"] as!String, todate: content["todate"] as!String, email: content["email"] as! String, regno: content["regno"] as! String, branch: content["branch"] as!String))
                    }
                    self.txtbranch.text = self.ReqVat[0].branch
                    self.txtdetails.text = self.ReqVat[0].details
                    self.txtemail.text = self.ReqVat[0].email
                    self.txtfrom.text = self.ReqVat[0].fromdate
                    self.txtid.text = self.ReqVat[0].id
                    self.txtmessage.text = self.ReqVat[0].message
                    self.txtregno.text = self.ReqVat[0].regno
                    self.txtto.text = self.ReqVat[0].todate
                }
                catch{
                    print(error)
                }
            }
            
        }
    }
}

