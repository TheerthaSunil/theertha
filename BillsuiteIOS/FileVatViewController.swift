//
//  FileVatViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 26/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
struct reg:Decodable,Encodable {
    var regno:String?
}
class FileVatViewController: UIViewController,DataSource ,hotelSendData{
    var defaults = UserDefaults.standard
    var filePath = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("reg.plist")
    func getHotel(hotelname: String) {
        txthotel.text = hotelname
        print("........")
        print(hotelname)
    }
    var regData:[reg] = [reg]()
    var username1:String?
    var password1:String?
    var tag:Int?
    var flag:Int?
    func getDate(date: String) {
        if tag == 1{
            txtfrm.text = date
        }
        else{
            txtto.text = date
        }
    }
    
    @IBOutlet var txtreg: UITextField!
    @IBOutlet var txtemail: UITextField!
    @IBOutlet var txtmsg: UITextField!
    @IBOutlet var txthotel: UITextField!
    @IBOutlet var txtto: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfrm.isUserInteractionEnabled = false
        txtto.isUserInteractionEnabled = false
        txtfrm.attributedPlaceholder = NSAttributedString(string: "Select From Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtto.attributedPlaceholder = NSAttributedString(string: "Select To Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        var dataStore = dataFromUserdefaults()
        username1 = dataStore["name"]
        password1 = dataStore["pass"]
        print(password1!)
        
    }
    @IBOutlet var txtfrm: UITextField!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func btnhotel(_ sender: UIButton) {
        print("xfdghjk")
        if let storyboard = storyboard{
            let vc = storyboard.instantiateViewController(withIdentifier:"MerchantVC") as! MerchantCustomViewController
            vc.delegate1 = self
            
            vc.pass = 1
            
            present(vc, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnfrom(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btnto(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnreg(_ sender: UIButton) {
        getFileVat()
    }
    func getFileVat() {
        let url =  "http://nuvactech-001-site13.etempurl.com/ios_filevat.php"
        Alamofire.request(url, method: .post, parameters: ["from":txtfrm.text!,"to":txtto.text!,"username" : username1!,"password" : password1!,"details":"vatreport","message":txtmsg.text!,"hotel":txthotel.text!,"email":txtemail.text!]).responseData { (res) in
            if res.result.isSuccess{
                print("success")
                do{
                    let result = try JSONDecoder().decode(reg.self, from: res.data!)
                    print(result)
                    if let reg = result.regno{
                        print("reg")
                    }
                    else{
                        print("data illa")
                        self.present(alert(message: "invalid date", Title: "not found"), animated: true, completion: nil)
                        return
                    }
                    self.txtreg.text = result.regno!
                    let regDetails = reg(regno: result.regno!)
                    let encoder = PropertyListEncoder()
                    do{
                        let encodedData = try encoder.encode(regDetails.self)
                        try encodedData.write(to: self.filePath!)
                        print(encodedData)
                        print(self.filePath)
                        print(FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask))
                    }catch{
                        print("Error while encoding \(error.localizedDescription)")
                    }
                }
                catch{
                    
                    print(error)
                }
                
            }
        }
    }
}
