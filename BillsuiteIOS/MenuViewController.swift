//
//  MenuViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 18/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//

import UIKit
protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index: Int32)
}
class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var cellid:Int?
    var list : [String] = ["Homepage","Viewstaff","purchase report","sales report", "vat report","filevate","request vat","history","Settings","logout"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableviewlist.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = list[indexPath.row]
        return cell!
    }
    var btnMenu : UIButton!
    var delegate : SlideMenuDelegate?
    
    @IBOutlet var tableviewlist: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableviewlist.dataSource = self
        tableviewlist.delegate = self
        tableviewlist.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if list[indexPath.row] == "Homepage"{
            self.performSegue(withIdentifier: "tohome", sender: self)
        }
        else if list[indexPath.row] == "Viewstaff"{
            self.performSegue(withIdentifier: "tostaff", sender: self)
            
        }
        else if list[indexPath.row] == "purchase report"{
            self.cellid = 0
            self.performSegue(withIdentifier: "topurchase", sender: self)
        }
        else if list[indexPath.row] == "sales report"{
            self.cellid = 1
            self.performSegue(withIdentifier: "topurchase", sender: self)
        }
            
        else if list[indexPath.row] == "vat report"{
            self.performSegue(withIdentifier: "tovatrpt", sender: self)
        }
        else if list[indexPath.row] == "filevate"{
            self.performSegue(withIdentifier: "tofilevat", sender: self)
        }
        else if list[indexPath.row] == "request vat"{
            self.performSegue(withIdentifier: "tovatreq", sender: self)
        }
        else if list[indexPath.row] == "history"{
            self.performSegue(withIdentifier: "tohistory", sender: self)
        }
        else if list[indexPath.row] == "Settings"{
            self.performSegue(withIdentifier: "tosetting", sender: self)
        }
        else if list[indexPath.row] == "logout"{
            self.performSegue(withIdentifier: "logout", sender: self)
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? PurchaselistViewController
        vc?.flag = cellid
    }
}
