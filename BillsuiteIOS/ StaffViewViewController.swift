//
//  StaffViewViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 21/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//

struct staffDetail {
    var id:String?
    var Name:String?
    var Gender: String?
    var Address:String?
    var Phone:String?
    var Destination:String?
    var Remarks:String?
    var Firm_id:String?
    var image:String?
}
import UIKit
import Alamofire
import UIKit
import IQKeyboardManagerSwift

class StaffViewViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return staffDetails.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellmycell", for: indexPath) as! MyCustomeCollectionViewCell
        cell.labtitle.text = staffDetails[indexPath.row].Name
        cell.lbladdr.text = staffDetails[indexPath.row].Address
        cell.lbldest.text = staffDetails[indexPath.row].Phone
        cell.lblid.text = staffDetails[indexPath.row].id
        cell.imagePhoto.image = base64Convert(base64String: staffDetails[indexPath.row].image)
        return cell
    }
    func base64Convert(base64String: String?) -> UIImage{
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "no_image_found")
        }else {
            // !!! Separation part is optional, depends on your Base64String !!!
            let temp = base64String?.components(separatedBy: ",")
            let dataDecoded : Data = Data(base64Encoded: temp![0], options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage!
        }
    }
    @IBOutlet var staffcollection: UICollectionView!
    var  staffDetails:[staffDetail] = [staffDetail]()
    var deta:[String:String] = [String:String]()
    var username2:String = ""
    var password2:String?
    
    var datas:[saveData] = [saveData]()
    var filePath = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first?.appendingPathComponent("MyNotes.plist")
    override func viewDidLoad() {
        super.viewDidLoad()
        staffcollection.dataSource = self
        staffcollection.delegate = self
        let decoder = PropertyListDecoder()
        do{
            let dataToDecode = try Data(contentsOf: filePath!)
            
            let datas = try decoder.decode(saveData.self, from: dataToDecode)
            print( datas.name!)
            username2 = datas.name!
            print(username2)
            password2 = datas.pass!
            print(datas)
        }catch{
            print(error)
        }
        
        StaffData()
    }
    func StaffData(){
        let url = "http://nuvactech-001-site13.etempurl.com/ios_new_viewstaff.php"
        Alamofire.request(url, method: HTTPMethod.post, parameters:["username":username2,"password":password2!]).responseJSON { (res) in
            if res.result.isSuccess{
                print("success")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as AnyObject
                    print(result)
                    let data = result["staff"] as AnyObject
                    let dataa = data as! [AnyObject]
                    for currentDetails in dataa
                    {
                        print(dataa[0]["id"])
                        self.staffDetails.append(staffDetail(id:dataa[0]["id"] as? String, Name: dataa[0]["Name"] as? String, Gender: dataa[0]["Gender"]as? String, Address: dataa[0]["Address"] as? String, Phone: dataa[0]["Phone"]as? String, Destination: dataa[0]["Destination"] as? String, Remarks: dataa[0]["Remarks"] as? String, Firm_id: dataa[0]["Firm_id"] as? String, image: dataa[0]["Image"] as? String))
                    }
                    self.staffcollection.reloadData()
                }
                    
                catch{
                    print("Error\(error.localizedDescription)")
                }
            }
            else{
                print("........")
            }
        }
        
    }
    
}
