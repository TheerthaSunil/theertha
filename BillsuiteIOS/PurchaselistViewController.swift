//
//  PurchaselistViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 22/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//
struct salesData{
    var id:String?
    var Invoice_Number: String?
    var vat_total: String
    var amount:String?
    var status: String?
    var table_id : String?
    var client_id:String?
    var staff_id: String?
}
struct PurchaseData:Decodable {
    var id:Int?
    var invoiceno:Int?
    var amount:Float?
    var vat:Float?
    var paymentmode:String?
    var remark:String?
    var docname:String?
    var branchid:Int?
}
import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
class PurchaselistViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,merchantSentData,DataSource,hotelSendData {
    var sale:[salesData] = [salesData]()
    func getHotel(hotelname: String) {
        txtmerchant.text = hotelname
        print(".......")
        print(hotelname)
    }
    var purchaseDetail:[PurchaseData] = [PurchaseData]()
    var flag:Int?
    var tag:Int?
    func getDate(date: String) {
        if tag == 1{
            txtfrom.text = date
        }
        else {
            txtto.text = date
        }
    }
    
    @IBOutlet var txtfrom: UITextField!
    @IBOutlet var txtto: UITextField!
    func getName(name: String) {
        txtmerchant.text = name
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var countnum:Int?
        if flag == 0{
            countnum = purchaseDetail.count
        }
        else if flag == 1{
            countnum = sale.count
        }
        return countnum!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "merchantcell")as? purchaseTableViewCell
        if flag == 0{
            cell?.lblinvoice1.text = purchaseDetail[indexPath.row].invoiceno as? String
            cell?.lblamount1.text = purchaseDetail[indexPath.row].amount as? String
            cell?.lblvat1.text = purchaseDetail[indexPath.row].vat as? String
            cell?.lblpay1.text = purchaseDetail[indexPath.row].paymentmode
            cell?.lblremark1.text = purchaseDetail[indexPath.row].remark
        }
        else if flag == 1{
            cell?.lblinvoice1.text = sale[indexPath.row].Invoice_Number as? String
            cell?.lblamount1.text = sale[indexPath.row].amount
            cell?.lblvat1.text = sale[indexPath.row].vat_total
            cell?.lblremark1.text = sale[indexPath.row].status
        }
        cell?.backgroundColor = .clear
        return cell!
    }
    @IBOutlet var merchanttbl: UITableView!
    @IBOutlet var txtmerchant: UITextField!
    var username2:String = ""
    var password2:String?
    @IBOutlet var btnselection: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfrom.isUserInteractionEnabled = false
        txtto.isUserInteractionEnabled = false
        txtfrom.attributedPlaceholder = NSAttributedString(string: "Select From Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtto.attributedPlaceholder = NSAttributedString(string: "Select From Date",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        merchanttbl.backgroundColor = .clear
        merchanttbl.isHidden = true
        if flag == 0 {
            btnselection.setTitle("Merchant", for: .normal)
        }
        else if flag == 1{
            btnselection.setTitle("Hotel", for: .normal)
        }
        var dataStore =  dataFromUserdefaults()
        username2 = dataStore["name"]!
        password2 = dataStore["pass"]
        print(username2)
    }
    
    @IBAction func btnmerchant(_ sender: UIButton) {
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"MerchantVC") as! MerchantCustomViewController
            vc.delegate = self
            vc.delegate1 = self
            if flag == 0{
                vc.pass = 0
            }
            else if flag == 1{
                vc.pass = 1
            }
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnfrom(_ sender: UIButton) {
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnto(_ sender: UIButton) {
        
        tag = sender.tag
        if let storyboard = storyboard
        {
            let vc = storyboard.instantiateViewController(withIdentifier:"datepopup") as! dateViewController
            vc.data = self
            
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSearch(_ sender: UIButton) {
        if flag == 0{
            print("searching.............")
            getPurchase()
        }
        else if flag == 1 {
            getSales()
        }
    }
    func getPurchase()  {
        let url =  "http://nuvactech-001-site13.etempurl.com/ios_new_purchase_report.php"
        print(username2)
        print(password2)
        print(txtfrom.text!)
        print(txtto.text!)
        print(txtmerchant.text!)
        Alamofire.request(url, method: HTTPMethod.post, parameters: ["username":username2,"password":password2!,"from":txtfrom.text!,"to":txtto.text!,"client":txtmerchant.text!]).responseData { (res) in
            if res.result.isSuccess{
                print(Parameters.self)
                print("success .....")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as AnyObject
                    if let status = result["status"]! as? String{
                        print("no data")
                        self.purchaseDetail.removeAll()
                        self.merchanttbl.reloadData()
                        let msgbox = alert(message: "Invalid date",Title : "No data found")
                        self.present(msgbox, animated: true, completion: nil)
                        return
                    }
                    else{
                        print("data found")
                    }
                    print("sagliaudcal")
                    let purchase = result["purchase"] as AnyObject
                    let purchaseobject = purchase as AnyObject
                    let datas = JSON( purchaseobject)
                    let res = datas.array?.count as! Int
                    print(res)
                    for n in 0...res-1{
                        let rowData = datas[n].dictionaryObject
                        print(rowData!["id"]!)
                        self.purchaseDetail.append((PurchaseData(id: rowData!["id"] as? Int, invoiceno: rowData!["invoiceno"] as? Int, amount: rowData!["amount"] as? Float, vat: rowData!["vat"] as? Float, paymentmode: (rowData!["paymentmode"] as? String), remark: rowData!["remark"] as? String, docname: rowData!["doname"] as?String, branchid: rowData!["branchid"]as? Int)))
                    }
                    self.merchanttbl.reloadData()
                    self.merchanttbl.isHidden = false
                }
                catch{
                    print("Error\(error.localizedDescription)")
                }
            }
            else{
                print("failed")
            }
        }
    }
    
    func getSales(){
        let url = "http://nuvactech-001-site13.etempurl.com/ios_new_sales_report.php"
        Alamofire.request(url, method: HTTPMethod.post, parameters: ["username":username2,"password":password2!,"from":txtfrom.text!,"to":txtto.text!,"client":txtmerchant.text!]).responseData { (res) in
            if res.result.isSuccess{
                print("success .....")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as AnyObject
                    print(result)
                    if let status = result["status"]! as? String{
                        print("data ella")
                        self.sale.removeAll()
                        self.merchanttbl.reloadData()
                        let msgbox = alert(message: "Invalid date",Title : "No data found")
                        self.present(msgbox, animated: true, completion: nil)
                        return
                    }
                    else{
                        print("data ende")
                    }
                    let sales = result["sales"] as AnyObject
                    print(sales)
                    let purchaseObject = sales as AnyObject
                    let datas = JSON(purchaseObject)
                    let res = datas.array?.count as! Int
                    print(res)
                    for n in 0...res-1{
                        let rowData = datas[n].dictionaryObject
                        print(rowData!["id"]!)
                        self.sale.append(salesData(id: rowData!["id"] as? String, Invoice_Number: rowData!["Invoice_Number"] as? String, vat_total: (rowData!["vat_total"] as? String)!, amount: rowData!["amount"] as? String, status:rowData!["status"] as? String, table_id:rowData!["table_id"] as? String, client_id: rowData!["client_id"] as? String, staff_id: rowData!["staff_id"]as? String))
                        print(self.sale)
                    }
                    self.merchanttbl.reloadData()
                    self.merchanttbl.isHidden = false
                }
                catch{
                    print(error)
                }
            }
        }
    }
}
