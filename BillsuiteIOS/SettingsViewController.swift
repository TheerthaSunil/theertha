//
//  SettingsViewController.swift
//  BillsuiteIOS
//
//  Created by Nuvac Technologies on 27/09/18.
//  Copyright © 2018 Nuvac Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift

class SettingsViewController: UIViewController {
    var username1:String?
    var password1:String?
    @IBOutlet var txtnew: UITextField!
    @IBOutlet var txtpass: UITextField!
    @IBOutlet var txtuser: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtuser.attributedPlaceholder = NSAttributedString(string: "Enter UserName",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtpass.attributedPlaceholder = NSAttributedString(string: "Enter Your  old Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtnew.attributedPlaceholder = NSAttributedString(string: "Enter Your new Password",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        var dataStore = dataFromUserdefaults()
        username1 = dataStore["name"]
        password1 = dataStore["pass"]
        print(password1!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func btnchange(_ sender: UIButton) {
        changepassword()
    }
    func changepassword(){
        let url = "http://nuvactech-001-site13.etempurl.com/ios_settings.php"
        Alamofire.request(url, method: .post, parameters: ["username" : username1!,"password" : password1!,"newpassword":txtnew.text!]).responseData { (res) in
            if res.result.isSuccess{
                print("success")
                do{
                    let result = try JSONSerialization.jsonObject(with: res.data!) as! AnyObject
                    print(result)
                    let data = result["status"] as! String
                    print(data)
                    let alert = UIAlertController(title: "Alert", message: "Successfully Updated", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.performSegue(withIdentifier: "tologin", sender: self)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                catch{
                    print(error)
                }
            }
        }
    }
}
